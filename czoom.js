(function ($) {
  Drupal.behaviors.cZoom = {
    attach: function (context, settings) {
      
      $('#czoom').toggleClass('nojs js');

      CloudZoom.quickStart();
      $("#czoom a > img").unwrap();
      
      if(jQuery().Thumbelina) {
        $('#thumbs').Thumbelina({
           $bwdBut: $('#thumbs .top'),
           $fwdBut: $('#thumbs .bottom'),
           orientation: 'vertical',
           easing: settings.cZoomThumbelina.easing,
           maxSpeed: settings.cZoomThumbelina.maxSpeed,
        });
      }
    }
  }
})(jQuery);