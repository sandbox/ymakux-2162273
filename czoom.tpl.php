<div id="czoom" class="nojs">
  <?php if($thumbs): ?>
    <div id="thumbs">
      <div class="thumbelina-but vert top">&#708;</div>
      <ul class="thumbelina">
        <?php foreach ($thumbs as $delta => $thumb): ?>
          <li>
            <a href="<?php print $thumb["url"]["original"]; ?>" target="_blank">
              <img class="cloudzoom-gallery" src="<?php print $thumb["url"]["style"]; ?>" data-cloudzoom='<?php print json_encode($thumb['settings']); ?>'>
            </a>
          </li>    
        <?php endforeach; ?>
      </ul>
      <div class="thumbelina-but vert bottom">&#709;</div>
    </div>
  <?php endif; ?>
  <?php if($main_image): ?>
    <div class="image">
      <a href="<?php print $main_image["url"]["original"]; ?>" target="_blank">
        <img class="cloudzoom" alt="<?php print $main_image["alt"]; ?>" id="zoom" src="<?php print $main_image["url"]["style"]; ?>" data-cloudzoom='<?php print json_encode($main_image["settings"]); ?>' />
      </a>
    </div>
  <?php endif; ?>
</div>
